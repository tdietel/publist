from setuptools import setup

setup(
    name='publist',
    version='0.2',
    py_modules=['publist'],
    install_requires=[
        'Click',
        'cachecontrol[filecache]'
    ],
    entry_points='''
        [console_scripts]
        publist=publist:cli
    ''',
)
