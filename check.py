#!/usr/bin/env python3

import yaml
import argparse

#---------------------------------------------------------------------
# Define check names and options
checks = {
    #'doi':    { 'short': 'd', 'default': True },
    #'title':  { 'short': 't', 'default': True },
    #'issn':   { 'short': 'i', 'default': True },
    'dupdoi': { 'short': 'D', 'default': True },
}

#---------------------------------------------------------------------
# Argument parser

parser = argparse.ArgumentParser(description='Check YAML article data.',
                                 prefix_chars='-+')

parser.add_argument('file', type=argparse.FileType('r'),
                    help='YAML file to be checked')

for opt,data in checks.items():

    parser.add_argument('--check-'+opt, '+'+data['short'], dest="chk_"+opt,
                        action="store_true", default=data['default'])

    parser.add_argument('--no-check-'+opt, '-'+data['short'], dest="chk_"+opt,
                        action="store_false", default=data['default'])
    



    
args = parser.parse_args()
#print(args)

#exit(1)

#---------------------------------------------------------------------
# Read the yaml file

papers = yaml.safe_load(args.file)
#print(papers)

#---------------------------------------------------------------------
# print a paper for consistent logging
def printpaper(p):
    print ("    ", p['year'], p['title'])


#---------------------------------------------------------------------
# find duplicate DOIs and titles
dois = [r['doi'] for r in papers if 'doi' in r]
dupdois = set([x for x in dois if dois.count(x) > 1])

titles = [r['title'] for r in papers if 'title' in r]
duptitles = set([x for x in titles if titles.count(x) > 1])

#---------------------------------------------------------------------
# initialize error info/stats

errorstats = {
    'doi':   { 'desc': 'Missing DOI',       'type': 'field' },
    'title': { 'desc': 'Missing title',     'type': 'field' },
    'issn':  { 'desc': 'Missing ISSN/ISBN', 'type': 'field' },

    'dupdoi':   { 'desc': 'Duplicate DOI', 'type': 'dupl' },
    'duptitle': { 'desc': 'Duplicate title', 'type': 'dupl' },
}

for e in errorstats:
    errorstats[e]['count'] = 0



#---------------------------------------------------------------------
# initialize error counters

for entry in papers:

    errors = []
    
    # check missing fields
    #for f in ['title', 'doi', 'issn']:
    for f in (x for x in errorstats if errorstats[x]['type']=='field'):
        if f not in entry: errors.append(f)
            
    # check duplicate DOI
    if 'doi' in entry and entry['doi'] in dupdois:
        errors.append('dupdoi')
            
    # check duplicate title
    if 'title' in entry and entry['title'] in duptitles:
        errors.append('duptitle')
            

    if errors != []:
        print ('-----------------------------------------------------------')
        if 'title' in entry:
            print ('Title: ', entry['title'])
        else:
            print ('Title:  --- MISSING ---')

        if 'journal' in entry and 'volume' in entry:
            print ('Ref:   %s %s ' % (entry['journal'], entry['volume']) )


    if errors != []:
        print()

    for e in errors:
        print (errorstats[e]['desc'])
        errorstats[e]['count'] += 1

    if errors != []:
        print()
        print(entry)



print ('-----------------------------------------------------------')
print ()
print ('-----------------------------------------------------------')

for e in errorstats:
    print ("%20s :   %5d" % (errorstats[e]['desc'], errorstats[e]['count'] ) )

print ()
print ("%20s :   %5d  entries in %s"
       % ("Total", len(papers), args.file.name))

print ('-----------------------------------------------------------')

print ("Papers by year")

for y in sorted(set([int(i['year']) for i in papers])):
    print("%7d :   %5d papers" % (y,len([x for x in papers if int(x['year'])==y])))

print ()

print ('-----------------------------------------------------------')
print ()
print ()

    
