
publist - Publications Lists in Python
======================================

Overview
--------

This Python module handles lists of publications. The main purpose the
maintenance of the publication list in the `NRF database`_. It
interacts with the database through the web interface and can add and
update papers. The module also supports the handling of publication
lists in Python, including importing various formats, filtering,
validity checking etc.

.. _`NRF database`: http://nrfsubmission.nrf.ac.za/


Installation with VirtualEnv
----------------------------
```
python3 -m venv venv
. venv/bin/activate
which pip
pip install --editable .
```


Usage
-----
```
publist
publist find a dietel and tc p
```


Authors
-------
- Sahal Yacoob <sahal.yacoob@uct.ac.za>
- Tom Dietel <tom@dietel.net>
