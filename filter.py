#!/usr/bin/env python3

import yaml
import difflib
import argparse
import collections

DiffResults = collections.namedtuple("DiffResults", "new mod zombie doi")

def diff_all_online_papers(allpapers, onlpapers):

    #---------------------------------------------------------------------
    # create lists to hold new and modified papers
    newpapers = []
    modpapers = []
    zompapers = [] # zombie papers, online DOI not found in allpapers

    doimatches = [] # online papers without DOI information

    alldois = []
    alltitles = []


    # loop over all my papers
    for entry in allpapers:

        entry['year'] = int(entry['year'])


        #if int(entry['year']) < 2015: continue

        if 'doi' not in entry:
            print ("ERROR: paper found without DOI")
            print (entry)
            exit

        alldois.append( entry['doi'] )
        alltitles.append( entry['title'] )

        # check if the DOI exists int he online papers
        #https://stackoverflow.com/questions/8653516/python-list-of-dictionaries-search
        #onlentry = next( (item for item in onlpapers if item['doi'] == entry['doi']), None )

        onlentry = None

        for i in onlpapers:
            if 'doi' in i and i['doi']==entry['doi']:
                #print ( "DOI   %s" % entry['doi'])
                onlentry = i

        if onlentry is None:

            #if entry['year']>=2010 and entry['year']<2019:
            newpapers.append(entry)

        else:
            mis = []
            for f in ['title', 'doi', 'year', 'journal', 'volume']:
                if entry[f] != onlentry[f]: mis.append(f)

            if mis != []:
                modpapers.append(entry)
                #print("MOD   %-50s " % entry['doi'], mis)



    for onlentry in onlpapers:

        if 'title' not in onlentry: continue
        if 'year' not in onlentry: continue

        #if 'doi' not in onlentry: print(onlentry)


        # find the matching paper in allpapers
        if 'doi' in onlentry:

            if onlentry['doi'] in alldois:
                # all fine: online entry has DOI that matches allpapers
                continue

            else:
                print("ERROR: online DOI not found in allpapers: ", onlentry['doi'])
                #print(onlentry)
                zompapers.append(onlentry)
                #exit(-1)
                continue

        elif 'title' in onlentry and onlentry['title'] in alltitles:
            for entry in allpapers:
                if onlentry['title'] == entry['title']:
                    #print ("===============================================")
                    #print ("TITLE MATCH")
                    #print (onlentry)
                    #print (entry)
                    #onlentry['doi'] = entry['doi']
                    doimatches.append( {
                        'title':   onlentry['title'],
                        'journal': onlentry['journal'],
                        'volume':  onlentry['volume'],
                        'cand': [ { 'doi': entry['doi'], 'title': entry['title'] } ]
                    } )

        else:

            # We have an online paper without DOI
            #doimatch.append(onlentry)

            #print ("===============================================")
            #print ("Looking for matches for")
            #print ()
            #print (onlentry['title'])
            #print ()
            ##print (onlentry)
            ##print ()
            #print ("possible matches:")
            #print ()

            candidates = {}
            checkfields = ['journal', 'volume', 'year']

            for ipaper in allpapers:

                matchfields = [x for x in checkfields if ipaper[x]==onlentry[x]]

                if checkfields == matchfields:
                    candidates[ipaper['title']] = ipaper['doi']
                    #print (ipaper)


            if len(candidates) == 0:
                print ("===============================")
                print ("ERROR: no match found for entry")
                print ()
                print (onlentry)
                print()

            canddata = []
            for c in difflib.get_close_matches(onlentry['title'], candidates):
                canddata.append( { 'title': c, 'doi': candidates[c] } )

            doimatches.append( {
                'title':   onlentry['title'],
                'journal': onlentry['journal'],
                'volume':  onlentry['volume'],
                'cand': canddata
            } )

    return DiffResults(new=newpapers, mod=modpapers, zombie=zompapers, doi=doimatches)


#---------------------------------------------------------------------
# Argument parser

parser = argparse.ArgumentParser(description='Check YAML article data.',
                                 prefix_chars='-+')

parser.add_argument('infile', type=argparse.FileType('r'),
                    help='Full list of publications')

parser.add_argument('reffile', type=argparse.FileType('r'),
                    help='List of existing publications')


args = parser.parse_args()
#print(args)

#exit(1)

#allpapers_file = "dietel-20190131.yaml"
# allpapers_file = "dietel-all.yaml"
# onlpapers_file = "nrfcv-20190131.yaml"


#---------------------------------------------------------------------
# read the list of all my papers
# with open(allpapers_file) as f:
allpapers = yaml.safe_load(args.infile)

# read the list of all the papers on the NRF system
# with open(onlpapers_file) as f:
onlpapers = yaml.safe_load(args.reffile)

foo = diff_all_online_papers(allpapers,onlpapers)
print(foo)
exit


doi_match = [x for x in foo.doi if len(x['cand']) == 1]
doi_miss  = [x for x in foo.doi if len(x['cand']) == 0]
doi_multi = [x for x in foo.doi if len(x['cand'])  > 1]

for m in doi_match:
    print ("\nMATCH  ",m['title'])
    print ("       ",m['cand'][0]['title'])

for m in doi_multi:
    print ("\nMULTI  ",m['title'])
    print()
    for c in m['cand']:
        print ("       ",c['title'])

for m in doi_miss:
    print ("\nMISS   ",m['title'])

print()



print ("DOI matches:     ", len(doi_match))
print ("DOI misses:      ", len(doi_miss))
print ("DOI ambig.:      ", len(doi_multi))



with open('new.yaml', "w") as f:
    yaml.dump(foo.new, f)

with open('changed.yaml', "w") as f:
    yaml.dump(foo.mod, f)

with open('doi.yaml', "w") as f:
    yaml.dump(foo.doi, f)
