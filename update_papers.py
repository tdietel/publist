#!/usr/bin/env python3

import nrf
import yaml

# ----------------------------------------------------------------------------
# variables - these should be settable on the command line

#infile = "dietel-20180214.publist"
#infile = "/Users/tom/private/publist/changed.yaml"
infile = "/Users/tom/private/publist/dietel-20200207.yaml"

# updatefields = ['issn','start_page','end_page'],
# the author field is always updated
updatefields = [],
firstpage = 2
lastpage = 100

# ----------------------------------------------------------------------------
# load nrf_update and start web connection

nu = nrf.uploader()
nu.launch_browser()


# ----------------------------------------------------------------------------
# load the data file and update all entries

with open(infile) as f:

    # use safe_load instead load
    allpapers = yaml.safe_load(f)

    # let nrf_uploader handle the
    nu.update_entries(allpapers, updatefields, firstpage, lastpage)
