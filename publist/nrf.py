#!/usr/bin/env python3
#
# Sahal Yacoob, UKZN 2015/2018 <sahal.yacoob@uct.ac.za>
# Tom Dietel <tom@dietel.net> UCT 2016-2021
# Sahal Yacoob <sahal.yacoob@uct.ac.za> UCT 2018-- (I guess I should just edit the line above yours)
#
# The Year Picker only works if the brower controlled by the driver /
# selenium is the focus of the OS
# Please add your own text to the reasonv variable which will be used to
# populate 'Own Contribution'
# NEEDS geckodriver available in $PATH
# Download the PDF of your current CV from : https://nrfsubmission.nrf.ac.za/NrfMkII/CV/CvPrintPreview.aspx (link at the bottom of the page)
# run 'gs -sDEVICE=txtwrite -o %stdout% {downloaded_pdf} | grep 'Web Address' | grep 'doi.org'|sed 's/^.*.doi.org\///' > NRF.txt '
# use {NRF.txt} as 'NRF file'
#  export PYTHONIOENCODING='utf-8'
# export PATH=$PATH:/Users/sahal/cernbox/NRF\ Publication\ tools
# 2010 -- 2012 need pruning
##############################################################################
##############################################################################
#import os
#import pickle
#os.environ["PYTHONIOENCODING"]='utf-8'
#os.environ["PATH"] = (os.environ["PATH"]+":"+os.environ["PWD"])
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains # to control mouse
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import ElementClickInterceptedException,MoveTargetOutOfBoundsException,NoSuchElementException,TimeoutException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities #needed ?
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary #needed ?
caps = DesiredCapabilities.FIREFOX #needed ?
caps["marionette"] = True #needed ?

#import getpass
#import sys
from time import sleep
#import json
import urllib.request
import traceback
import getpass
import os

class BrowserCommException(Exception):
    """Exception raised for communication errors with the web browser.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message


##############################################################################
##############################################################################
##
## publist.nrf.uploader class
##
##############################################################################
##############################################################################

class uploader:

    #reasontext = 'co-owner of intellectual property of research'
    verbose = False # allow / suppress per item output
    clean = False  # if true, no new entries are added, and all downloaded entried go into update dictionary
    #autodelete = False # will delete entries without user intervention (not advised)
    #collaboration_format_choice = 1
    #collaboration_format = {1: 'ATLAS/ALICE Collaboration (as appropriate)', 2: 'Dietel, T. et al. ALICE Collaboration'}
    #delay= 0 #for webpage to update
    #p_skip = 0 #  // 2012 starts on page 48
    entries = {}
    #myname = ''
    #update_dictionary = False
    #rewrite_dictionary = True
    process_raw = True


    # ==========================================================================
    def __init__(self):
        #self.fail_file = open('Failure','w',encoding='utf-8')
        #self.success_file = open('Success','w',encoding='utf-8')
        #print ('nrf_uploader Configuration:')
        #print ('The delay to allow websites to load is:' + str(self.delay) )
        #print ('I know of the following journals:')
        #print ('Currently Getting DOI from Web Address Field to evaluate updates')
        pass

    # ==========================================================================
    def launch_browser(self, my_username=None, my_password=None):

        # Determine user name and password
        if my_username is None:
            my_username = os.getenv('NRF_USERNAME', None)

        if my_password is None:
            my_password = os.getenv('NRF_PASSWORD', None)

        if my_username==None or my_password==None:
            print ("NRF_USERNAME and/or NRF_PASSWORD not set.",
                   "Doing manual login.")
            print ("To automate:",
                   "export NRF_USERNAME=\"<user>\";",
                   "read -s NRF_PASSWORD; export NRF_PASSWORD")

        if my_username == None:
            my_username = input('NRF Username: ')

        if my_password == None:
            my_password = getpass.getpass('NRF Password: ')

        # Pick your Browser I use Firefox
        self.binary = FirefoxBinary('/Applications/Firefox.app/Contents/MacOS/firefox')
        self.driver = webdriver.Firefox(firefox_binary=self.binary)
        self.driver.get("https://nrfsubmission.nrf.ac.za/NrfMkII/CV/JournalRefreedPeerReviewedArticle.aspx")
        nrf_username = self.driver.find_element_by_id("ctl00_cphContent_txtNumber")
        nrf_password = self.driver.find_element_by_id("txtPassword")
        login_button = self.driver.find_element_by_id("ctl00_cphContent_cmdLogin")
        nrf_username.send_keys(my_username)
        nrf_password.send_keys(my_password)
        login_button.click()
        self.driver.implicitly_wait(30)

    # ==========================================================================
    def __del__(self):
        try:
            self.driver.close()
        except:
            return

    # ==========================================================================
    def skip_pages(self,n_skip):
        for skip in range(0, n_skip):
            sleep(1.0)
            self.click_on_field("rgPageNext",'CN')

    # ==========================================================================
    def wait_for_field(self,fieldname,type='ID'):
        #print ("Waiting for field '%s'..." %fieldname)
        if type == 'ID':
            try:
                elem = WebDriverWait(self.driver, 100).until(EC.element_to_be_clickable(
                    ( By.ID,
                      "ctl00_ctl00_cphContent_CphOutputs_%s" % fieldname)))
                return elem
            except TimeoutException:
                raise
            except:
                sys.exit('timeout')
        if type == 'CN':
            try:
                elem = WebDriverWait(self.driver, 100).until(EC.element_to_be_clickable(
                    ( By.CLASS_NAME,
                        "%s" % fieldname)))
                return elem
            except TimeoutException:
                print ('timeout')
                raise
            except:
                sys.exit('timeout')
        else:
            return
        #print ("Found '%s'..." %fieldname)
        return elem

    # ==========================================================================
    def click_on_field(self,fieldname, type='ID',delay=1,maxretries=10):
        try:
            sleep(delay)
            elem = self.wait_for_field(fieldname,type)
            elem.click()
        except ElementClickInterceptedException:

            if maxretries>0:
                #print("waiting...")
                self.click_on_field(fieldname, type, delay, maxretries-1)
            else:
                input('Browser waiting for user intervention, ' +
                      'or you got here too quickly, increase the delay. ' +
                      'PRESS RETURN KEY TO TRY ' + str(fieldname) + ' AGAIN')
                self.click_on_field(fieldname, type, delay*3, 0)

        except (NoSuchElementException, TimeoutException):
            raise
        return elem

    # ==========================================================================
    def clear_and_fill_entry_field(self,fieldname,content):
        elem = self.driver.find_element_by_id("ctl00_ctl00_cphContent_CphOutputs_WinEdit_C_Txt%s" % fieldname)
        #print ("Sending '%s' to %s" % (content,fieldname))
        elem.clear()
        elem.send_keys(content)
        #input("step")

    # ==========================================================================
    def fill_entry_field(self,fieldname,content):
        elem = self.driver.find_element_by_id("ctl00_ctl00_cphContent_CphOutputs_WinEdit_C_Txt%s" % fieldname)
        #print ("Sending '%s' to %s" % (content,fieldname))
        elem.send_keys(content)
        #input("step")

    # ==========================================================================
    def read_entry_field(self,fieldname):
        elem = self.driver.find_element_by_id("ctl00_ctl00_cphContent_CphOutputs_WinEdit_C_Txt%s" % fieldname)
        if fieldname == 'Doi':
            return elem.get_attribute("value")
        return elem.text
    #input("step")

    # ==========================================================================
    def fill_year(self,content):
        elem = self.driver.find_element_by_id("ctl00_ctl00_cphContent_CphOutputs_WinEdit_C_TxtYear")
        actions=ActionChains(self.driver)
        sleep(0.1)
        try:
            actions.move_to_element(elem).click().perform()
        except MoveTargetOutOfBoundsException:
            input("TESTING -- Press Enter to continue...")
        except:
            traceback.print_exc()
        sleep(1)
        for i in range (10):
            try:
                self.driver.find_element_by_xpath('//label[text()="%d"]' % content).click()
                return
            except NoSuchElementException:
                sleep(2.0)
                #elem = self.driver.find_element_by_id("LeftButtonctl00_ctl00_cphContent_CphOutputs_WinEdit_C_TxtYear").click()
                elem = self.driver.find_element_by_id("ctl00_ctl00_cphContent_CphOutputs_WinEdit_C_TxtYear").click()
        print (' I could not pick the year')
        raise nrf.WebCommError("unable to pick the year")


    #sys.exit()
        #print ("Sending '%s' to %s" % (content,fieldname))

    # ==========================================================================
    def get_val(self,val,object):
        try:
            if type(object) == type([]):
                if val == 'author':
                    returnval = []
                    for item in object:
                        if self.map[val] in item:
                            returnval.append(item[self.map[val]])
                    return (' and '.join(returnval))
                else:
                    for item in object:
                        if self.map[val] in item:
                            return item[self.map[val]]
            if self.map[val] in object:
                if type(object[self.map[val]]) == type([]):
                    if val == 'doi':
                        return object[self.map[val]]
                    else:
                        return object[self.map[val]][0]
                else:
                    return object[self.map[val]]
        except:
            if val == 'end_page':
                return
            elif val == 'publisher':
                return ''
            else:
                print("nrf_uploader::get_val Unexpected error:", sys.exc_info()[0])
                print (val + ' ' + self.map[val])
                print (object)
                traceback.print_exc()
                raise


    # ==========================================================================
    def add_entry(self,entry):
        #elem = self.wait_for_field("CmdAdd")
        #elem.click()
        self.click_on_field("CmdAdd")
        # self.driver.switch_to_default_content()

        status = self.wait_for_field("WinEdit_C_CmbStatus_Input")
        status.send_keys('Pu')
        sleep(0.5)
        try:
            status.click()
        except ElementClickInterceptedException:
            sleep(2)
            status.click()
        except:
            traceback.print_exc()

        sleep(2.0)
        status.send_keys(Keys.RETURN)
        sleep(0.5)
        self.driver.implicitly_wait(1)
        # self.driver.switch_to_default_content()
        self.wait_for_field("WinEdit_C_TxtTitleOfArticle")

        fields = ['TitleOfArticle', 'TitleOfJournal', 'Volume', 'Doi',
        'IssnIsbnNumber', 'Publisher', 'OtherAuthors',
        'PageFrom', 'PageTo'
        ]

        for i in fields:
            self.fill_entry_field(i, entry.get(i))

        self.fill_year(entry.get('year'))


        save = self.click_on_field("WinEdit_C_CmdSave")
        if self.verbose:
            print (','.join(entry['doi']) + " Successfully Added")



    # ==========================================================================
    def update_doi(self, findpaper, firstpage, lastpage=-1):

        if lastpage<0: lastpage = firstpage

        self.skip_pages(firstpage - 1)
        thispage = firstpage

        unknownpapers = []

        #completed_on_this_page =[]
        while thispage <= lastpage:

            print ( "# processing page %d" % thispage )

            for var in ['04','06','08','10','12','14','16','18','20','22']:

                # load editing form for next article
                # self.driver.switch_to_default_content()
                try:
                    sleep(self.delay+1) # needs a slightly longer delay
                    status = self.click_on_field("RgPeerReviews_ctl00_ctl"+var+"_CmdEdit")
                except (NoSuchElementException):
                    print ('reached last page, all done')
                    return
                except:
                    print ("caught an exception while loading edit form")
                    traceback.print_exc()
                    return unknownpapers

                # wait for editing form to appear
                # self.driver.switch_to_default_content()
                self.wait_for_field("WinEdit_C_TxtTitleOfArticle")

                rdoi = self.read_entry_field('Doi')

                if rdoi != '':
                    # We have a DOI on the system, return to list
                    print ("ok   %s" % rdoi)

                    try:
                        self.wait_for_field("WinEdit_C_CmdCancelEdit")
                        sleep(self.delay)
                        self.click_on_field("WinEdit_C_CmdCancelEdit")

                    except:
                        print ("caught an exception while returning to paper list")
                        traceback.print_exc()
                        return

                    continue

                # --------------------------------------------------
                # This entry does not have a DOI, insert it

                # get some information about the article from the form
                title = self.read_entry_field('TitleOfArticle')

                # find the entry using the find hook
                entry = findpaper( {
                    'title': self.read_entry_field('TitleOfArticle')
                } )

                if entry is None:
                    print ("???  %s" % title)

                    try:
                        self.wait_for_field("WinEdit_C_CmdCancelEdit")
                        sleep(self.delay)
                        self.click_on_field("WinEdit_C_CmdCancelEdit")

                    except:
                        print (":exception: return to paper list")
                        traceback.print_exc()
                        return unknownpapers

                    continue

                # update the form and save
                try:

                    print ("UPD  %s" % entry['doi'])

                    self.wait_for_field("WinEdit_C_CmbStatus_Input")
                    self.clear_and_fill_entry_field('Doi', entry['doi'])

                    if 'issn' in entry:
                        self.clear_and_fill_entry_field('IssnIsbnNumber',
                                                        entry['issn'])
                    #self.clear_and_fill_entry_field('TitleOfArticle', entry['title'])
                    #self.clear_and_fill_entry_field('TitleOfJournal', entry['journal'])

                    if 'publisher' in entry:
                        self.clear_and_fill_entry_field('Publisher',
                                                        entry['publisher'])

                    #self.clear_and_fill_entry_field('PageFrom',
                    #                                entry['start_page'])
                    #self.clear_and_fill_entry_field('PageTo',
                    #                                entry['end_page'])
                    #self.clear_and_fill_entry_field('WebAddress', 'http://doi.org/'+entry['doi'])

                    sleep(self.delay)

                    # REAL: save changes
                    self.click_on_field("WinEdit_C_CmdSave")

                    ## DUMMY: cancel the edit
                    #self.click_on_field("WinEdit_C_CmdCancelEdit")


                except:
                    print ("exception: update DOI")
                    traceback.print_exc()
                    return


            # ------------------------------------------------------------
            # Done with the page loop, load next page
            try:
                #self.driver.switch_to_default_content()
                #if (rdoi==end_entry):
                #    print ("Last Entry Updated")
                #    return pages

                sleep(self.delay)
                #completed_on_this_page=[]
                self.click_on_field("rgPageNext",'CN')
                # self.driver.switch_to_default_content()
                sleep(self.delay+2)
                thispage += 1
                #pages = pages+1
            except:
                testsss = input('hold')
                print ("could not go to next page: Exiting loop after next try")

    # ==========================================================================
    def update_entries(self, allpapers, limitfields, firstpage, lastpage=-1):

        if lastpage<0: lastpage = firstpage

        self.skip_pages(firstpage - 1)
        thispage = firstpage

        while thispage <= lastpage:

            print ( "# processing page %d" % thispage )

            for var in ['04','06','08','10','12','14','16','18','20','22']:

                # load editing form for next article
                # self.driver.switch_to_default_content()
                try:
                    #sleep(self.delay+1) # needs a slightly longer delay
                    status = self.click_on_field("RgPeerReviews_ctl00_ctl"+var+"_CmdEdit")
                except (NoSuchElementException):
                    print ('reached last page, all done')
                    return
                except:
                    print ("caught an exception while loading edit form")
                    traceback.print_exc()
                    return unknownpapers

                # wait for editing form to appear
                # self.driver.switch_to_default_content()
                self.wait_for_field("WinEdit_C_TxtTitleOfArticle")

                rdoi = self.read_entry_field('Doi')

                if rdoi == '':
                    # We do not have a DOI on the system, return to list
                    print ("!!!  %s" % rdoi)

                    try:
                        #self.wait_for_field("WinEdit_C_CmdCancelEdit")
                        #sleep(self.delay)
                        self.click_on_field("WinEdit_C_CmdCancelEdit")

                    except:
                        print ("caught an exception while returning to paper list")
                        traceback.print_exc()
                        return

                    continue

                # --------------------------------------------------
                # Retrieve info from allpapers using DOI

                entry = next( (i for i in allpapers if i['doi'] == rdoi), None )

                if entry is None:
                    print ("???  %s" % title)

                    try:
                        #self.wait_for_field("WinEdit_C_CmdCancelEdit")
                        #sleep(self.delay)
                        self.click_on_field("WinEdit_C_CmdCancelEdit")

                    except:
                        print (":exception: return to paper list")
                        traceback.print_exc()
                        return #unknownpapers

                    continue

                # update the form and save
                try:

                    print ("UPD  %s" % entry['doi'])
                    self.wait_for_field("WinEdit_C_CmbStatus_Input")

                    # fill the fields listed in limitfields
                    # for f in limitfields:
                    #     if f in entry:
                    #         self.clear_and_fill_entry_field( self.nrfmap[f], entry[f])

                    # the year needs some special treatment
                    #self.fill_year(entry['year'])

                    # we need to convert the auther list into a string
                    self.clear_and_fill_entry_field('OtherAuthors', '\n'.join(entry['authors']))

                    # REAL: save changes
                    self.click_on_field("WinEdit_C_CmdSave")

                    ## DUMMY: cancel the edit
                    #sleep(10)
                    #self.click_on_field("WinEdit_C_CmdCancelEdit")

                except:
                    print ("exception: update DOI")
                    traceback.print_exc()
                    return


            # ------------------------------------------------------------
            # Done with the page loop, load next page
            try:
                #self.driver.switch_to_default_content()
                #if (rdoi==end_entry):
                #    print ("Last Entry Updated")
                #    return pages

                sleep(1)
                #completed_on_this_page=[]
                self.click_on_field("rgPageNext",'CN')
                # self.driver.switch_to_default_content()
                sleep(3)
                thispage += 1
                #pages = pages+1
            except:
                testsss = input('hold')
                print ("could not go to next page: Exiting loop after next try")
