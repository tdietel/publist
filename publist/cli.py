import click
import json
import yaml
from pprint import pprint
import datetime

import publist

# import publist.inspire
# import inspire
from . import nrf as nrfmod

@click.group()
def cli():
    pass

@cli.command()
@click.argument("name")
def saved_search(name):
    """List saved searches.

    If the NAME is 'list', print all saved searches. Otherwise, just print
    the requested one."""

    if name=="list":
        print()
        print("Saved searches:")
        print()
        for k,v in get_search_string().items():
            print(f"  {k:12s}  {v}")
        print()

    else:
        print(get_search_string(name))

def get_search_string(query=None):

    today = datetime.date.today()
    y2 = today - datetime.timedelta(2*365)

    searches = dict(
      dietel = "a dietel and tc p",
      dietel2y = f'a dietel and tc p and date > {y2.strftime("%Y-%m-%d")}',
    )

    if query is None:
        return searches

    elif len(query) == 0:
        return searches['test']

    elif len(query) == 1:
        return searches[query[0]]

    else:
        return " ".join(query)


@cli.command()
@click.argument("query",nargs=-1)
def quick_find(query):
    """Run fast query on Inspire HEP.

    The command shows DOI, title and number of citations for the queried
    articles. The QUERY follows the Inspire HEP query format.
    """

    papers = publist.inspire.quick_search(get_search_string(query))
    for p in papers:
        meta = p['metadata']

        if 'dois' in meta:
            print(f"{meta['earliest_date']:12s} {meta['dois'][0]['value']}")
        else:
            pprint(meta)



@cli.command()
@click.argument("query",nargs=-1)
# @click.option("-N", "--only-new",default=False)
@click.option("-N", "--new-since", type=click.File('rb'), default=None)
def find(query,new_since):
    """Run fast query on Inspire HEP.

    The command shows DOI, title and number of citations for the queried
    articles. The QUERY follows the Inspire HEP query format.
    """

    # if only_new:
    #     with open("nrfcv-20210202.yaml") as f:
    #         data = yaml.safe_load(f)

    #     nrfdois = [ i['doi'] for i in data ]

    if new_since is not None:
        nrfdois = [ i['doi'] for i in yaml.safe_load(new_since) ]
    else:
        nrfdois = []

    plist = publist.inspire.cached_query(get_search_string(query))
    for doi in plist:

        if doi in nrfdois:
            continue

        print()
        pprint(plist[doi]._asdict())

@cli.command()
@click.argument("query",nargs=-1)
@click.option("-o","--outfile", nargs=1)
def yamlfind(query,outfile):
    """Run fast query on Inspire HEP and return YAML file."""

    plist = publist.inspire.cached_query(get_search_string(query))

    ylist = {}
    for doi in plist:
        ylist[doi] = plist[doi]._asdict()

    with open(outfile,"w") as f:
        f.write(yaml.dump(dict(publications=ylist)))



@cli.group()
@click.pass_context
def nrf(ctx):
    nu = nrfmod.uploader()
    nu.launch_browser()

    ctx.ensure_object(dict)
    ctx.obj['NRF'] = nu


@nrf.command()
@click.argument("query",nargs=-1)
@click.option('-n', '--dry-run', is_flag=True)
@click.option("-N", "--new-since", type=click.File('rb'))
@click.pass_context
def add_new(ctx, query, new_since, dry_run):
    """Run query and add new results to NRF."""

    fields = ['TitleOfArticle', 'TitleOfJournal', 'Volume', 'Doi',
    'IssnIsbnNumber', 'Publisher', 'OtherAuthors',
    'PageFrom', 'PageTo'
    ]

    nu = ctx.obj['NRF']

    # Read list of existing DOIs from previously downloaded file
    nrfdois = [ i['doi'] for i in yaml.safe_load(new_since) ]

    plist = publist.inspire.cached_query(get_search_string(query))
    for doi in plist:

        if doi in nrfdois:
            continue

        p = plist[doi]
        # pprint(p)
        print()
        for f in fields:
            print(f"{f:20s} {p.get(f)}")

        if not dry_run:
            nu.add_entry(p)
