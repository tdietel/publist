#!/usr/bin/env python3

import click
import json
import yaml
import pathlib
from pprint import pprint
from datetime import datetime

import requests
from cachecontrol import CacheControl
from cachecontrol.caches.file_cache import FileCache

import publist

cache = CacheControl(requests.Session(), cache=FileCache('.web_cache'))


def get_preferred_value(data,prefs=()):

    if len(data) == 1:
        return data[0]['value']

    ref = data[0]['value']
    allmatch = True
    for x in data[1:]:
        if ref != x['value']:
            # pprint(data)
            allmatch=False
            # raise ValueError(f"mismatch '{i}' is supported at this time")

    if allmatch:
        return ref

    for x in data:
        if 'source' not in x:
            return x['value']

    for src in prefs:
        for x in data:
            if x['source'] == src:
                return x['value']

    pprint(data)
    raise ValueError(f"no match found for '{i}'")


def get_detail(id):

    url = f"https://inspirehep.net/api/literature/{id}"

    fields = [ "earliest_date", "dois", "citation_count",
               "collaborations", "authors.full_name",
               "document_type", "publication_info", "documents" ]

    r = cache.get(url, params=dict(fields=",".join(fields)))

    paper = r.json()

    try:

        dois = [ i['value'] for i in paper['metadata']['dois'] ]

        # maybe one should figure out in a better way how to get the best title
        title = paper['metadata']['titles'][0]['title']

        doi = get_preferred_value(paper['metadata']['dois'],('bibmatch','arXiv'))

        # what about multiple publications infos?

    except KeyError:
        pprint(paper['metadata'].keys())
        raise

    try:

        if len(paper['metadata']['collaborations']) > 1:
            pprint(paper['metadata']['collaborations'])
            raise ValueError(f"only a single collaboration is supported at this time")

        collaboration = paper['metadata']['collaborations'][0]['value']

    except KeyError:
        collaboration = None
        # pprint(paper['metadata'].keys())
        # raise


    try:
        if 'imprints' in paper['metadata'] and 'date' in paper['metadata']['imprints'][0]:
            date = paper['metadata']['imprints'][0]['date']
        elif 'preprint_date' in paper['metadata']:
            date = paper['metadata']['preprint_date']
        else:
            showdata = paper['metadata']
            for i in ('authors','references','abstracts','figures'):
                showdata[i] = '*** REMOVED ***'
            pprint(showdata)
            # pprint(paper['metadata']['imprints'])
            raise ValueError(f"publication date not found")

    except KeyError:
        pprint(paper['metadata'].keys())
        raise

    try:
        date = datetime.strptime(date,"%Y-%m-%d").date()
    except ValueError:
        try:
            date = datetime.strptime(date,"%Y-%m").date()
        except ValueError:
            date = datetime.strptime(date,"%Y").date()


    try:
        pubinfo = paper['metadata']['publication_info'][0]
        if len(paper['metadata']['publication_info']) > 1:
            print("WARNING: more than one instance of publication_info")
            pprint(paper['metadata']['publication_info'])

        if 'artid' in pubinfo:
            pages = pubinfo['artid']
        else:
            pages = pubinfo['page_start'] + "-" + pubinfo['page_end']

    except KeyError:
        pprint(pubinfo.keys())
        raise

    bibitem = cache.get(url, params=dict(format='latex-eu')).text.split('\n')
    latexauthor = bibitem[2]
    latexref = bibitem[4]

    if latexauthor.endswith(','):
        latexauthor = latexauthor[:-1]


    return publist.PubItem(
         doi=doi, date=date, inspire=paper['id'],
         arxiv = "",
         first_author = paper['metadata']['authors'][0]['full_name'],
         citation_count = paper['metadata']['citation_count'],
         journal = pubinfo['journal_title'],
         volume = pubinfo['journal_volume'],
         pages = pages,
         year = pubinfo['year'],
         collaboration = collaboration,
         title = title,
         latexauthor = latexauthor,
         latexref = latexref
    )


def quick_search(q):
    url = "https://inspirehep.net/api/literature"
    fields = "earliest_date,dois,citation_count"
    r = requests.get(url, params=dict(q=q,size=1000,fields=fields))

    return r.json()['hits']['hits']



class cached_query:
    """Main query interface for Inspire HEP

    This class is a convenient interface to run a query on Inspire HEP. It runs
    a fast query to update the list of available articles for a query, and then
    uses a local cache to speed up the lookup of details for a publication.

    The class provides iterators to loop over the papers like a dict, with the
    DOI ask keys and PubItems as values.
    """

    def __init__(self, query, cachefile="~/.publist/cache/pubitems.yaml"):
        """Constructor for cached_query.

        Loads the cache file into memory and runs the fast query.
        """

        self.cachefile = pathlib.Path(cachefile).expanduser()
        if self.cachefile.is_file():
            with open(self.cachefile) as f:
                self.papers = yaml.load(f, Loader=yaml.FullLoader)

        if not hasattr(self,'papers') or self.papers is None:
            self.papers = dict()

        self.idx = dict()
        for i in quick_search(query):
            doi = i['metadata']['dois'][0]['value']
            id = i['id']

            self.idx[doi] = id

    def __del__(self):
        """Destructor for cached_query

        Writes the in-memory cache to the cache file.
        """

        self.cachefile.parent.mkdir(exist_ok=True,parents=True)
        with open(self.cachefile, "w") as f:
            yaml.dump(self.papers,f)

    def __iter__(self):
        self.itr = iter(self.idx)
        return self

    def __next__(self):
        return next(self.itr)

    def __getitem__(self, doi):

        if doi not in self.papers:
            print(f"retrieve DOI {doi}")
            self.papers[doi] = get_detail(self.idx[doi])

        return self.papers[doi]
