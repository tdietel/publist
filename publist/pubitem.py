
from typing import NamedTuple
import datetime
import yaml

class PubItem(NamedTuple):
    doi: str
    title: str
    inspire: int
    arxiv: str
    citation_count: int
    date: datetime.date
    first_author: str
    collaboration: str
    journal: str
    volume: str
    year: int
    pages: str
    latexauthor: str
    latexref: str

    def get(self,attr):
        if attr == 'TitleOfArticle': return self.title
        if attr == 'TitleOfJournal': return self.journal
        if attr == 'Volume': return self.volume
        if attr == 'Doi': return self.doi
        if attr == 'year': return self.year

        if attr == 'IssnIsbnNumber':
            jdata = journaldata[self.journal]

            if isinstance(jdata['ISSN'], dict):
                return jdata['ISSN'][entry['volume'][0]]
            else:
                return jdata['ISSN']

        if attr == 'Publisher':
            return journaldata[self.journal]['Publisher']

        if attr == 'OtherAuthors':

            idx = self.first_author.find(",") + 3
            return "\n".join([self.first_author[:idx],"Dietel, T", "ALICE Collaboration"])

        if attr == 'PageFrom':
            if '-' in self.pages:
                return self.pages.split('-')[0]
            else:
                return self.pages

        if attr == 'PageTo':
            if '-' in self.pages:
                return self.pages.split('-')[1]
            else:
                return ""


def pubitem_constructor(loader, node):
    items = loader.construct_sequence(node)
    return PubItem(*items)

yaml.add_constructor('tag:yaml.org,2002:python/object/new:publist.pubitem.PubItem', pubitem_constructor)




# ==========================================================================
journaldata = {
    'JHEP' : {
        'Publisher' : 'Springer',
        'ISSN':'1029-8479'},
    'JINST' : {
        'Publisher' : 'IOP Publishing',
        'ISSN':'1748-02213'},
    'Eur.Phys.J.' : {
        'Publisher' : 'EDP Sciences, Societa Italiana di Fisica, Springer',
        'ISSN':{'C': '1434-6052'}},
    'Eur.Phys.J.C' : {
        'Publisher' : 'EDP Sciences, Societa Italiana di Fisica, Springer',
        'ISSN': '1434-6052'},
    'Eur.Phys.J.Plus' : {
        'Publisher' : 'EDP Sciences, Societa Italiana di Fisica, Springer',
        'ISSN': '2190-5444' },
    'Nature Phys.' : {
        'Publisher' : 'Nature Publishing Group',
        'ISSN':'1745-2473'},

    'Int.J.Mod.Phys.': { 'Publisher' : 'World Scientific', 'ISSN': '0217-751X'},
    'JCAP' : { 'Publisher' : 'IOP Publishing', 'ISSN': '1475-7516' },
    'J.Phys.': { 'Publisher': 'IOP Publishing', 'ISSN': { 'G': '0954-3899' } },
    'J.Phys.G': { 'Publisher': 'IOP Publishing', 'ISSN': '0954-3899' },
    'Phys.Rev.Lett.' : {
        'Publisher' : 'American Physical Society',
        'ISSN':'0031-9007'},
    'Phys.Rev.' : {
        'Publisher' : 'American Physical Society',
        'ISSN':{'C' :'2469-9985', 'D':'2470-0010' }},
    'Phys.Rev.C' : { 'Publisher' : 'American Physical Society', 'ISSN': '2469-9985' },
    'Phys.Rev.D' : { 'Publisher' : 'American Physical Society', 'ISSN': '2470-0010' },
    'Phys.Lett.' : { 'Publisher' : 'Elsevier', 'ISSN':'0370-2693'},
    'Phys.Lett.B' : { 'Publisher' : 'Elsevier', 'ISSN':'0370-2693'},
    'Nucl.Phys.' : {
        'Publisher' : 'Elsevier',
        'ISSN' : {'A' :'0375-9474', 'B':'0550-3213' }},
    'Nucl.Phys.A' : { 'Publisher' : 'Elsevier', 'ISSN' : '0375-9474' },
    'Nucl.Phys.B' : { 'Publisher' : 'Elsevier', 'ISSN' : '0550-3213' },
    'New J.Phys.' : { 'Publisher' : 'IOP Publishing', 'ISSN':'1367-2630'},
    'Science'     : {
        'Publisher' : 'American Association for the Advancement of Science (AAAS)',
        'ISSN': '1095-9203'},
    'Nucl.Instrum.Meth.' : {
        'Publisher' : 'Elsevier',
        'ISSN' : {'A' : '0168-9002', 'B' : '0168-583X'}},

    'Nucl.Instrum.Meth.A' : { 'Publisher' : 'Elsevier', 'ISSN' : '0168-9002' },
    'Nucl.Instrum.Meth.B' : { 'Publisher' : 'Elsevier', 'ISSN' : '0168-583X' },
    'Comput.Phys.Commun.': { 'Publisher' : 'Elsevier', 'ISSN' : '0010-4655' },
    'Nature': { 'Publisher': 'Nature Research', 'ISSN': '0028-0836' },
}
