#!/usr/bin/env python3

import subprocess
import re
import yaml
import argparse

#---------------------------------------------------------------------
# Argument parser

parser = argparse.ArgumentParser(description='Convert NRF CV to Yaml file.')

parser.add_argument('infile', type=str, help='Input PDF file')

parser.add_argument('outfile', type=argparse.FileType('w'),
                    help='Output YAML file')


args = parser.parse_args()

# exit(1)


#---------------------------------------------------------------------
# Read data
indata = subprocess.run( "gs -sDEVICE=txtwrite -o %stdout% " + args.infile + " | uniq",
                         shell=True, stdout=subprocess.PIPE).stdout.decode('utf-8')


re_ignore = dict( (s, re.compile(r)) for s,r in {
    'header': '^\s*Dr.*Thomas.*Dietel\s*Page',
    'footer': '^\s*Page\s+\d+\s*$',
}.items() )



# Regular expressions to match section titles
re_sect = dict( (s, re.compile(r)) for s,r in {
    'proceedings': '^\s*Refereed/Peer-reviewed Conference Outputs$',
    'articles':    '^\s*Articles in Refereed/Peer-reviewed Journals$',
    'books':       '^\s*Books edited by Applicant',
}.items() )

# Regular expressions to match article fields
re_field = dict( (s, re.compile('^(\s+%s\s*)(.*)'%r)) for s,r in {
    'title':      'Title of Article',
    'journal':    'Title of Journal',
    'authors':    'All Authors in Order Appearing on Output',
    'volume':     'Volume',
    'year':       'Year',
    'doi':        'DOI',
    'issn':       'ISSN\/ISBN Number',
    'reason':     'Applicant\'s Contribution',
    'publisher':  'Publisher',
    'status':     'Status',
    'start_page': 'Page From',
    'end_page':   'Page To',
}.items() )

# helper function to find matches
def find_first_match(res,txt):
    for label,regexp in res.items():
        if regexp.match(txt):
            return label
    return False



# set up data structures for paper data
articles = []
data = {}


# initialize loop variables
section = ''
lastfield = ''
offset = 0


# loop over the input file
for line in indata.splitlines():

    #print (">>>", line)

    # igore headers, footers...
    if find_first_match(re_ignore,line):
        continue

    # determine the sections
    lbl = find_first_match(re_sect,line)
    if lbl:
        section = lbl
        print("SECTION: ", section)
        continue


    # loop over articles
    if section=='articles':
        #print (">>>", line)

        # check if we have a match
        field = find_first_match(re_field,line)

        if field:
            # if we found a match, we'll run the RE again to get the data

            m = re_field[field].match(line)
            offset = len(m.groups()[0])
            text = m.groups()[1]

            # detect a new item: we either find a ISSN/ISBN (always
            # the first field), or the field already exists in our data
            if (field=='issn' and data!={}) or field in data:
                print(data)
                articles.append(data)
                data = {}
                print ("----------------------")


            # now we process the actual data
            if field=='authors':
                data['authors'] = [text]
                lastfield = field
                print ("%10s: %s" % (field, text))

            elif field in ['year']:
                data[field] = int(text)
                lastfield = field
                print ("%10s: %s" % (field, text))

            elif field in ['issn', 'title', 'journal', 'volume',
                           'publisher', 'doi', 'reason',
                           'status', 'start_page', 'end_page']:
                data[field] = text
                lastfield = field
                print ("%10s: %s" % (field, text))

            else:
                print ("ERROR: don't know how to handle field '%s'"
                       % field)

        else:

            # this must be a continued field:
            field = lastfield
            text = line[offset:]
            #print ("cont: ", text)

            if field=='authors':
                data['authors'].append(text)

            elif field=='title' and data['title'][-1]=='-':
                data['title'] += text

            elif field=='title' and data['title'][-1]!='-':
                data['title'] += " " + text

            elif field in ['reason']:
                data[field] += "\n" + text


articles.append(data)
#print(data)


# with open(outfile, 'w') as of:
yaml.dump(articles, args.outfile, default_flow_style=False)
